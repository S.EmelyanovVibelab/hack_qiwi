package com.example.navigation

import android.annotation.SuppressLint
import androidx.navigation.NavController
import io.sentry.Breadcrumb
import io.sentry.Sentry
import io.sentry.SentryLevel
import timber.log.Timber

typealias navigateAction = () -> Unit

/**
 * Base Router class with base rout actions and default initializing
 *
 * All Module Router class must Extend this class
 */
open class Router {
    companion object {
        private var _navController: NavController? = null
        @SuppressLint("RestrictedApi")
        private val onDestinationChangedListener: NavController.OnDestinationChangedListener =
            NavController.OnDestinationChangedListener { controller, destination, _ ->
                Sentry.addBreadcrumb(Breadcrumb().apply {
                    level = SentryLevel.INFO
                    category = "navigation"
                    type = "navigation"
                    setData("to", destination.displayName)
                })
                Timber.d("Navigation to ${destination.navigatorName}, controller: $controller")
            }

        fun init(navController: NavController) {
            _navController = navController
            navController.removeOnDestinationChangedListener(onDestinationChangedListener)
            navController.addOnDestinationChangedListener(onDestinationChangedListener)
        }
    }

    val navController: NavController
        get() {
            if (_navController == null) throw (IllegalStateException("NavController is null"))
            else return _navController!!
        }
    protected fun navigate(navigateAction: navigateAction) {
        navigateAction.invoke()
    }

    fun popUp() {
        navigate { navController.popBackStack() }
    }

    fun popUpTo(destinationId: Int, isInclusive: Boolean = false) {
        navigate { navController.popBackStack(destinationId, isInclusive) }
    }
}
