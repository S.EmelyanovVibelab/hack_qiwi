package com.example.ui_components.extension

import android.widget.Button

fun Button.setEnable(isEnable: Boolean,color:Int,mode: Mode = Mode.SRC_ATOP){
    this.isEnabled = isEnable
    background.setColorFilter(color,mode)
}