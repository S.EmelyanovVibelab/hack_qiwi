package com.example.ui_components.ui_state


sealed class ViewState<out T : Any>(val data: T?) {
    data class Success<out T : Any>(val successData: T) : ViewState<T>(successData)
    data class Loading<out T : Any>(val localData: T?): ViewState<T>(localData)
    data class Error<out T : Any>(val localData: T?): ViewState<T>(localData)
    data class Default<out T : Any>(val localData: T?): ViewState<T>(localData)
}
