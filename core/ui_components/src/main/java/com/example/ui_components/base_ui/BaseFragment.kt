package com.example.ui_components.base_ui

import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.example.ui_components.R
import com.google.android.material.snackbar.Snackbar
import timber.log.Timber

abstract class BaseFragment<T:ViewBinding>(@LayoutRes idRes:Int):Fragment(idRes) {
    protected abstract val binding: T

    fun showSnackbar(text:String, duration:Int = Snackbar.LENGTH_SHORT){
        Snackbar.make(binding.root, text, duration).show()
    }

    fun navigate(router:() -> Unit){
        try {
            router.invoke()
        }catch(e: IllegalStateException) {
            Timber.e(e)
            showSnackbar(resources.getString(R.string.navigation_failed))
        } catch (e: IllegalArgumentException) {
            Timber.e(e)
        }
    }

}