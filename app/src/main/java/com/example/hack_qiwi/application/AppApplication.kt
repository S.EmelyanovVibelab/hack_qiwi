package com.example.hack_qiwi.application

import android.app.Application
import android.os.Build
import com.example.dependency.DepsMap
import com.example.dependency.HasDependencies
import com.example.hack_qiwi.BuildConfig
import com.example.hack_qiwi.config.SettingsConfig
import com.example.hack_qiwi.di.DaggerAppComponent
import javax.inject.Inject

class AppApplication : Application(),
    HasDependencies
{
    @Inject
    override lateinit var depsMap: DepsMap

    override fun onCreate() {
        super.onCreate()
        DaggerAppComponent.builder()
            .application(this)
            .settingsConfig(
                SettingsConfig(
                Build.VERSION.SDK_INT,
                BuildConfig.APPLICATION_ID,
                BuildConfig.VERSION_CODE,
                Build.BRAND,
                Build.DEVICE,
                Build.MODEL
            )
            )
            .build()
            .inject(this)
    }

}