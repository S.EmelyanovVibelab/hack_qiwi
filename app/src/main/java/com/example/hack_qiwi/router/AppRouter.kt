package com.example.hack_qiwi.router

import android.os.Bundle
import com.example.hack_qiwi.R
import com.example.navigation.Router
import com.example.payment_qiwi.PaymentQiwiConst

object AppRouter:Router() {
    fun navigateToScanQr(value: String) {
        val data = Bundle().apply {
            putString(PaymentQiwiConst.PRICE_KEY, value)
        }
        navigate {
            navController.navigate(
                R.id.action_payFragmentPreview_to_fragmentQrScan,
                data
            )
        }
    }

    fun navigateToGenerateQr(){
        navigate {
            navController.navigate(R.id.action_payFragmentPreview_to_fragmentQrGenerate)
        }
    }
}