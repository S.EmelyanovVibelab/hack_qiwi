package com.example.hack_qiwi.di.modules

import android.content.Context
import com.example.back_qiwi.api.QiwiApi
import com.example.data.repository.PaymentQiwiRepositoryImpl
import com.example.domain_payment.repository.PaymentQiwiRepository
import com.example.hack_qiwi.R
import com.example.utils.AuthManager
import dagger.Module
import dagger.Provides

@Module(includes = [NetworkModule::class])
class DataModule {
    @Provides
    fun providersPaymentQiwiRepository(
        api: QiwiApi,
        auth: AuthManager,
        context: Context,
    ): PaymentQiwiRepository = PaymentQiwiRepositoryImpl(
        api,
        auth,
        context.resources.getString(com.example.payment_qiwi.R.string.error_message)
    )
}