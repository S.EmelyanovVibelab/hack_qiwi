package com.example.hack_qiwi.di.modules

import android.content.Context
import com.example.back_qiwi.api.QiwiApi
import com.example.back_qiwi.provider.QiwiProvider
import com.example.data.auth.AuthManagerImpl
import com.example.data.providers_api.PaymentsProvider
import com.example.utils.AuthManager
import com.example.utils.user_agent.UserAgent
import dagger.Module
import dagger.Provides


@Module
class NetworkModule {
    @Provides
    fun providersAuthManager(context: Context): AuthManager = AuthManagerImpl(context)

    @Provides
    fun providersQiwiProvider(
        manager: AuthManager,
        userAgent: UserAgent,
    ): QiwiProvider = QiwiProvider(
        manager,
        userAgent,
    )

    @Provides
    fun providersPaymentsProvider(
        qiwiProvider:QiwiProvider
    ): PaymentsProvider = PaymentsProvider(qiwiProvider)

    @Provides
    fun providersQiwiApi(provider:PaymentsProvider): QiwiApi = provider.provideQiwiPayment()
}