package com.example.hack_qiwi.di.modules

import com.example.domain_payment.interactor.PaymentQiwiInteractor
import com.example.domain.interactors.PaymentQiwiInteractorImpl
import com.example.domain_payment.repository.PaymentQiwiRepository
import dagger.Module
import dagger.Provides

@Module(includes = [DataModule::class])
class DomainModule {
    @Provides
    fun providersPaymentQiwiInteractor(
        repository: PaymentQiwiRepository
    ): PaymentQiwiInteractor = PaymentQiwiInteractorImpl(repository)

}