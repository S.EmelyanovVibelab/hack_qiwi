package com.example.hack_qiwi.di.feature_deps

import com.example.dagger.DependenciesKey
import com.example.dependency.Dependencies
import com.example.hack_qiwi.di.AppComponent
import com.example.payment_qiwi.di.PaymentQiwiDeps
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap


@Module
interface PaymentQiwiDepsModule {
    @Binds
    @IntoMap
    @DependenciesKey(PaymentQiwiDeps::class)
    fun bindPaymentQiwiDeps(impl: AppComponent): Dependencies
}