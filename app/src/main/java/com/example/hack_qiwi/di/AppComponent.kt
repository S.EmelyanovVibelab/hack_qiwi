package com.example.hack_qiwi.di

import android.app.Application
import android.content.Context
import com.example.data.user_agent.UserAgentImpl
import com.example.hack_qiwi.application.AppApplication
import com.example.hack_qiwi.config.SettingsConfig
import com.example.hack_qiwi.di.feature_deps.PaymentQiwiDepsModule
import com.example.hack_qiwi.di.modules.DomainModule
import com.example.payment_qiwi.di.PaymentQiwiDeps
import com.example.utils.user_agent.UserAgent
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.Provides
import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class AppScope

@Component(
    modules = [
        AppModule::class,
        DomainModule::class,
        PaymentQiwiDepsModule::class,
    ]
)
@AppScope
interface AppComponent: PaymentQiwiDeps {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        @BindsInstance
        fun settingsConfig(settingsConfig: SettingsConfig): Builder
        fun build(): AppComponent
    }

    fun inject(appApplication: AppApplication)
}

@Module
class AppModule {
    @AppScope
    @Provides
    fun provideContext(
        application: Application
    ): Context = application.applicationContext

    @Provides
    @AppScope
    fun userAgent(settingsConfig: SettingsConfig): UserAgent = with(settingsConfig) {
        UserAgentImpl(
            sdk, applicationId, versionCode, brand, device, model
        )
    }
}