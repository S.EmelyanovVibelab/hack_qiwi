package com.example.hack_qiwi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.hack_qiwi.databinding.ActivityMainBinding
import com.example.navigation.Router

class MainActivity : AppCompatActivity(R.layout.activity_main) {
    private val binding: ActivityMainBinding by viewBinding()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initNavigation()
    }

    private fun initNavigation(){
        Router.init(
            findNavController(R.id.nav_host_fragment)
        )
    }

    private fun initButton(){
        with(binding){

        }
    }
}