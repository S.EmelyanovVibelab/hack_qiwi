package com.example.hack_qiwi.fragment

import android.os.Bundle
import android.view.View
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.hack_qiwi.R
import com.example.hack_qiwi.databinding.FragmentPayPreviewBinding
import com.example.hack_qiwi.router.AppRouter
import com.example.ui_components.afterTextChanged
import com.example.ui_components.base_ui.BaseFragment
import com.example.ui_components.extension.Mode
import com.example.ui_components.extension.setEnable

class PayFragmentPreview :BaseFragment<FragmentPayPreviewBinding>(R.layout.fragment_pay_preview) {
    override val binding: FragmentPayPreviewBinding by viewBinding()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initButton()
        initView()
    }

    private fun initView(){
        with(binding){
            price.afterTextChanged{ value ->
                when{
                    value.isEmpty() -> {
                        buttonPay.setEnable(
                    false,
                            resources.getColor(com.example.ui_components.R.color.color_background_button_2),
                            Mode.SRC_IN,
                        )
                    }
                    value[0] == '0' -> {
                        buttonPay.setEnable(
                            false,
                            resources.getColor(com.example.ui_components.R.color.color_background_button_2),
                            Mode.SRC_IN,
                        )
                    }
                    else ->{
                        buttonPay.setEnable(
                            true,
                            resources.getColor(com.example.ui_components.R.color.color_background_button_1),
                            Mode.SRC_IN,
                        )
                    }
                }
            }
        }
    }

    private fun initButton(){
        with(binding){
            buttonPay.setOnClickListener {
                navigate {
                    AppRouter.navigateToScanQr(price.text.toString())
                }
            }
            buttonGenerateQr.setOnClickListener {
                navigate {
                    AppRouter.navigateToGenerateQr()
                }
            }
        }
    }
}