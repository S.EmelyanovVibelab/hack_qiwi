package com.example.hack_qiwi.config

data class SettingsConfig(
    val sdk:Int,
    val applicationId: String,
    val versionCode: Int,
    val brand: String,
    val device:String,
    val model: String
)