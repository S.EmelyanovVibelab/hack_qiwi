package com.example.data.repository

import com.example.back_qiwi.api.QiwiApi
import com.example.back_qiwi.dao.GenerateRequest
import com.example.data.base.BaseRepository
import com.example.data.mapper.asDomain
import com.example.domain_payment.base_entity.DataEntity
import com.example.domain_payment.entity.SmsEntity
import com.example.domain_payment.entity.SmsGenerateEntity
import com.example.domain_payment.repository.PaymentQiwiRepository
import com.example.utils.AuthManager
import com.example.utils.models.ResponseStatus
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class PaymentQiwiRepositoryImpl(
    private val api: QiwiApi,
    private val auth: AuthManager,
    private val errorMessage: String,
): BaseRepository(),PaymentQiwiRepository {
    override suspend fun getSms(value: SmsGenerateEntity): Flow<DataEntity<SmsEntity>> {
        return flow {
                when(
                val resp = safeApiSuspendResult {
                    api.generateSms(
                        GenerateRequest(
                            auth.phone,
                            value.requestId,
                            value.siteId,
                        )
                    )
                }
            ){
                is ResponseStatus.Success -> {
                    resp.data?.also {
                        emit(
                            DataEntity.Success(it.asDomain(),resp.code)
                        )
                    }
                }
                is ResponseStatus.LocalError ->{
                    if(resp.message.isEmpty()){
                        emit(
                            DataEntity.Error(errorMessage,null)
                        )
                    }else{
                        emit(
                            DataEntity.Error(resp.message,null)
                        )
                    }
                }
                is ResponseStatus.ServerError ->{
                    emit(
                        DataEntity.Error(resp.exception.message?:errorMessage,null)
                    )
                }
            }
        }
    }
}



