package com.example.data.user_agent

import com.example.utils.user_agent.UserAgent

data class UserAgentImpl(
    override val sdk:Int,
    override val applicationId: String,
    override val versionCode: Int,
    override val brand: String,
    override val device:String,
    override val model: String,
): UserAgent {
    override fun getResultStroke():String{
        return "Android: $sdk/ " +
                "$applicationId/ " +
                "build: $versionCode/ " +
                "$brand - $device - $model"
    }
}