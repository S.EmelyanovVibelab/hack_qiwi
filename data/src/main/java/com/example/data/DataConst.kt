package com.example.data

internal object DataConst {
    internal const val AUTH_STORE = "AUTH"
    internal const val AUTH_KEY = "AUTH_KEY_QIWI"
    internal const val PHONE_KEY = "PHONE_KEY_QIWI"
}