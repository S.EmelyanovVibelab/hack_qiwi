package com.example.data.mapper

import com.example.back_qiwi.dao.SmsResponse
import com.example.domain_payment.entity.SmsEntity

fun SmsResponse.asDomain(): SmsEntity {
    return SmsEntity(
        requestId = requestId,
        status = status,
    )
}