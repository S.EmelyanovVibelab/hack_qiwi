package com.example.data.auth

import android.content.Context
import com.example.data.DataConst
import com.example.utils.AuthManager

class AuthManagerImpl(context: Context): AuthManager {
    private val store = context.getSharedPreferences(DataConst.AUTH_STORE,Context.MODE_PRIVATE)
    override var token: String
        get() = store.getString(DataConst.AUTH_KEY,"")?:""
    set(value){
        store.edit().apply {
            putString(DataConst.AUTH_KEY, value)
            apply()
        }
    }

    override var phone: String
        get() = store.getString(DataConst.PHONE_KEY,"")?:""
        set(value){
            store.edit().apply {
                putString(DataConst.PHONE_KEY, value)
                apply()
            }
        }

}