package com.example.data.base
import com.example.utils.exeptions.*
import com.example.utils.models.ResponseStatus
import com.example.utils.models.ServerResponse
import java.net.UnknownHostException
import okhttp3.ResponseBody
import retrofit2.HttpException
import retrofit2.Response
import timber.log.Timber
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.google.gson.reflect.TypeToken
import com.example.utils.models.Error


typealias Request<T> = suspend () -> Response<ServerResponse<T>>

open class BaseRepository{
    @Deprecated("use safeApiSuspendResult")
    protected suspend fun <T : Any> safeApiCall(call: Request<T>): ResponseStatus<T> {
        return safeApiResult(call)
    }
    @Deprecated("use safeApiSuspendResult")
    private suspend fun <K : Any> safeApiResult(call: suspend () -> Response<ServerResponse<K>>): ResponseStatus<K> {
        val response: Response<ServerResponse<K>>
        try {
            response = call.invoke()
            if (response.isSuccessful) {
                return ResponseStatus.Success(response.body()?.data, response.code())
            }

            val errorBody: Error? = response.errorBody()?.parseError()
            return ResponseStatus.ServerError(
                NetworkException(
                    errorBody?.message,
                    Throwable(REPOSITORY),
                    errorBody?.code ?: 0
                )
            )
        } catch (e: Exception) {
            Timber.e(e)
            when (e) {
                is UnknownHostException -> return ResponseStatus.LocalError(
                    null,
                    NoNetworkException(cause = Throwable(REPOSITORY), message = e.message), NO_NETWORK_ERROR_CODE
                )
                is JsonSyntaxException -> {
                    Timber.e(e)
                    return ResponseStatus.ServerError(
                        NetworkException(
                            e.message,/*UtilContext.context.getString(R.string.NewRepository),*/
                            Throwable(REPOSITORY), PARSE_ERROR_CODE
                        )
                    )
                }
                is HttpException -> return ResponseStatus.LocalError(
                    null,
                    e.response()?.errorBody()?.parseError() ?: NullPointerException(),
                    e.code()
                )
                else -> return ResponseStatus.LocalError(null, e, LOCAL_ERROR_CODE)
            }
        }
    }

    protected suspend fun <K : Any> safeApiSuspendResult(call: suspend () -> ServerResponse<K>?): ResponseStatus<K> {
        val response: ServerResponse<K>?
        try {
            response = call.invoke()
            if (response != null) {
                return ResponseStatus.Success(response.data, response.code ?: NO_ERROR_CODE)
            }
            val errorBody: Error? = response?.parseError()
            return ResponseStatus.ServerError(
                NetworkException(
                    errorBody?.message,
                    Throwable(REPOSITORY),
                    errorBody?.code ?: NO_ERROR_CODE
                )
            )
        } catch (e: Exception) {
            Timber.e(e)
            when (e) {
                is UnknownHostException -> return ResponseStatus.LocalError(
                    null,
                    NoNetworkException(cause = Throwable(REPOSITORY), message = e.message), NO_NETWORK_ERROR_CODE
                )
                is JsonSyntaxException -> {
                    Timber.e(e)
                    return ResponseStatus.ServerError(
                        NetworkException(
                            /*UtilContext.context.getString(R.string.NewRepository)*/e.message,
                            Throwable(REPOSITORY), PARSE_ERROR_CODE
                        )
                    )
                }
                is HttpException -> return ResponseStatus.LocalError(
                    null,
                    e.response()?.errorBody()?.parseError() ?: NullPointerException(),
                    e.code(),
                    e.message()
                )
                else -> return ResponseStatus.LocalError(null, e, LOCAL_ERROR_CODE)
            }
        }
    }

    protected suspend fun <K : Any> safeApiSuspendResultNoResponse(call: suspend () -> Response<K>?): ResponseStatus<K> {
        val response: Response<K>?
        try {
            response = call.invoke()
            if (response != null && response.isSuccessful) {
                return ResponseStatus.Success(response.body(), response.code())
            }
            val errorBody: Error? = response?.errorBody()?.parseError()
            return ResponseStatus.ServerError(
                NetworkException(
                    errorBody?.message,
                    Throwable(REPOSITORY),
                    errorBody?.code ?: NO_ERROR_CODE
                )
            )
        } catch (e: Exception) {
            Timber.e(e)
            when (e) {
                is UnknownHostException -> return ResponseStatus.LocalError(
                    null,
                    NoNetworkException(cause = Throwable(REPOSITORY), message = e.message), NO_NETWORK_ERROR_CODE
                )
                is JsonSyntaxException -> {
                    Timber.e(e)
                    return ResponseStatus.ServerError(
                        NetworkException(
                            e.message,
                            Throwable(REPOSITORY), PARSE_ERROR_CODE
                        )
                    )
                }
                is HttpException -> return ResponseStatus.LocalError(
                    null,
                    e.response()?.errorBody()?.parseError() ?: NullPointerException(),
                    e.code()
                )
                else -> return ResponseStatus.LocalError(null, e, LOCAL_ERROR_CODE)
            }
        }
    }
    companion object{
        private const val REPOSITORY = "REPOSITORY"
    }
}

fun ResponseBody.parseError(): Error {
    return try {
        val gson = Gson()
        val type = object : TypeToken<Error>() {}.type
        gson.fromJson(charStream(), type)
    } catch (e: JsonSyntaxException) {
        Error("", PARSE_ERROR, e.message)
    }
}