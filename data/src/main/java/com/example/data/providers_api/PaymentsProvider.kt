package com.example.data.providers_api

import com.example.back_qiwi.api.QiwiApi
import com.example.back_qiwi.provider.QiwiProvider
import okhttp3.OkHttpClient

class PaymentsProvider(
    private val provider: QiwiProvider,
) {
    fun provideQiwiPayment(okHttpClient: OkHttpClient? = null): QiwiApi = provider.provideRetrofit(okHttpClient,QiwiApi::class.java)
}