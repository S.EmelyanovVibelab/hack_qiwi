# Описание проекта
Проект разрабатывался для хакатона для кейса по оплате для Qiwi
В проекте используется следующий стек:  
1. Di - dagger
2. Network - retrofit
3. Pattern on presentation layer - MVVM
4. Load image - Glide
5. Architecture - Clean Architecture
6. Coroutine, Flow, State Flow, Shared Flow
7. Data Store - Shared Preference

# Описание модулей 
1. app - модуль для сборки проекта
2. domain - бизнес логика приложения, там находится реализация interactors
3. data - модуль, где находятся реализация репозиториев, менеджеров и провайдер на REST API
4. network:utils - общие вещи для сетевого слоя
5. network:back_qiwi - сетевой слой для qiwi с провайдером api
6. feature:payments_qiwi и feature:payments_qiwi:domain_payments - фича для оплаты для qiwi, в первом модуле находится ui с внешними зависимостями на предоставление бизнес логики по интерфейсу, второй модуль интерфейсы бизнес логики с entity сущностями
7. core:dagger - модуль с анотацией для dagger для графа зависимостей 
8. core:dependency - модуль с механизмом поиска зависимостей в графе, а также механизм выстраивание зависимостей через map
9. core:navigation - содержит навигационный класс общий для всего проекта
10. core:ui_components - модуль с общими ui компонентами, стрингами, цветами и картинками