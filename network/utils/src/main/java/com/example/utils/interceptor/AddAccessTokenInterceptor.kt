package com.example.utils.interceptor

import okhttp3.Interceptor
import okhttp3.Protocol
import okhttp3.Response
import okhttp3.ResponseBody.Companion.toResponseBody
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class AddAccessTokenInterceptor(
    private var token: String = "",
) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        try {
            val builder = chain.request().newBuilder()
            val accessTokenHeader = "Authorization"
            return chain.proceed(
                if (chain.request().header(accessTokenHeader).isNullOrEmpty()) {
                    builder.header(accessTokenHeader, token).build()
                } else {
                    chain.request()
                }
            )
        } catch (e: UnknownHostException) {
            return Response.Builder()
                .request(chain.request())
                .protocol(Protocol.HTTP_1_1)
                .code(400)
                .body("${e.message}".toResponseBody(null))
                .message("${e.message}")
                .build()
        }catch (e: SocketTimeoutException){
            return Response.Builder()
                .request(chain.request())
                .protocol(Protocol.HTTP_1_1)
                .code(413)
                .body("${e.message}".toResponseBody(null))
                .message("${e.message}")
                .build()
        }
        catch (e: RuntimeException){
            return Response.Builder()
                .request(chain.request())
                .protocol(Protocol.HTTP_1_1)
                .code(413)
                .body("${e.message}".toResponseBody(null))
                .message("${e.message}")
                .build()
        }

    }
}