package com.example.utils

interface AuthManager {
    var token: String
    var phone: String
}