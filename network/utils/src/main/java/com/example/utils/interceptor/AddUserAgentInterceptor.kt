package com.example.utils.interceptor

import okhttp3.Interceptor
import okhttp3.Protocol
import okhttp3.Response
import okhttp3.ResponseBody.Companion.toResponseBody
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class AddUserAgentInterceptor(val userAgent: String) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        return try {
            val builder = chain.request().newBuilder()
            builder.header("User-Agent", userAgent)
            chain.proceed(builder.build())
        } catch (e: UnknownHostException) {
            Response.Builder()
                .request(chain.request())
                .protocol(Protocol.HTTP_1_1)
                .code(400)
                .body("${e.message}".toResponseBody(null))
                .message("${e.message}")
                .build()
        } catch (e: SocketTimeoutException){
            Response.Builder()
                .request(chain.request())
                .protocol(Protocol.HTTP_1_1)
                .code(413)
                .body("${e.message}".toResponseBody(null))
                .message("${e.message}")
                .build()
        }
        catch (e: RuntimeException){
            Response.Builder()
                .request(chain.request())
                .protocol(Protocol.HTTP_1_1)
                .code(413)
                .body("${e.message}".toResponseBody(null))
                .message("${e.message}")
                .build()
        }
    }
}