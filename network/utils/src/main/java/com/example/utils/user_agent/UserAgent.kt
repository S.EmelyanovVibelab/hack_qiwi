package com.example.utils.user_agent


interface UserAgent{
    val sdk:Int
    val applicationId: String
    val versionCode: Int
    val brand: String
    val device:String
    val model: String
    fun getResultStroke():String
}
