package com.example.back_qiwi.dao

import com.google.gson.annotations.SerializedName

data class PaymentResponse(
    @SerializedName("requestId")
    val requestId:String,
    @SerializedName("smsToken")
    val smsToken: SmsToken,
    @SerializedName("status")
    val status:Status,
    @SerializedName("token")
    val token:PaymentToken,

)

data class SmsToken(
    @SerializedName("expiredDate")
    val expiredDate:String,
    @SerializedName("token")
    val token:String,
)

data class PaymentToken(
    @SerializedName("expiredDate")
    val expiredDate: String,
    @SerializedName("value")
    val value:String,
)

data class Status(
    @SerializedName("value")
    val value:String,
)