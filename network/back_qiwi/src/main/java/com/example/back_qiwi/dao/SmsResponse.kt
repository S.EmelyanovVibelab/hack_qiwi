package com.example.back_qiwi.dao

import com.google.gson.annotations.SerializedName

data class SmsResponse(
    @SerializedName("requestId")
    val requestId: String,
    @SerializedName("status")
    val status: String,
)
