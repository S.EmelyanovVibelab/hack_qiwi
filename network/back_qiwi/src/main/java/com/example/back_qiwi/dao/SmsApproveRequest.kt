package com.example.back_qiwi.dao

import com.google.gson.annotations.SerializedName
import java.util.*

data class SmsApproveRequest(
    @SerializedName("requestId")
    val requestId: String,
    @SerializedName("smsCode")
    val smsCode: String,
)
