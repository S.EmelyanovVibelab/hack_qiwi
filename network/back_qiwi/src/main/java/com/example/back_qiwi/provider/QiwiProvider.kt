package com.example.back_qiwi.provider

import com.example.back_qiwi.QiwiConst
import com.example.utils.AuthManager
import com.example.utils.interceptor.AddAccessTokenInterceptor
import com.example.utils.interceptor.AddUserAgentInterceptor
import com.example.utils.user_agent.UserAgent
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import okhttp3.logging.HttpLoggingInterceptor

class QiwiProvider(
    private val authManager: AuthManager,
    private val userAgent: UserAgent,
) {
    private val gson = GsonBuilder().setLenient().create()
    private var defaultOkHttpClient: OkHttpClient? = null
    private val timeout: Long = 15

    fun <T> provideRetrofit(
        okHttpClient: OkHttpClient?,
        clazz: Class<T>,
        host: String = QiwiConst.HOST_QIWI
    ): T {
        val client = okHttpClient ?: provideOkHttpClient()
        return Retrofit.Builder()
            .baseUrl(host)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .create(clazz)
    }

    private fun provideOkHttpClient(): OkHttpClient {
        if (defaultOkHttpClient != null) return defaultOkHttpClient!!

        val interceptor: HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
            this.level = HttpLoggingInterceptor.Level.BODY
        }
        defaultOkHttpClient = OkHttpClient.Builder().apply {
            readTimeout(timeout, TimeUnit.SECONDS)
            connectTimeout(timeout, TimeUnit.SECONDS)
            addInterceptor(AddUserAgentInterceptor(userAgent.getResultStroke()))
            addInterceptor(AddAccessTokenInterceptor(authManager.token))
            addInterceptor(interceptor)
        }.build()
        return defaultOkHttpClient!!
    }

}