package com.example.back_qiwi.dao

import com.google.gson.annotations.SerializedName
import java.util.*

data class GenerateRequest(
    @SerializedName("phone")
    val phone: String,
    @SerializedName("requestId")
    val requestId: String,
    @SerializedName("siteId")
    val siteId: String,
)
