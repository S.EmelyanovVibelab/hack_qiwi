package com.example.back_qiwi.api

import com.example.back_qiwi.dao.GenerateRequest
import com.example.back_qiwi.dao.PaymentResponse
import com.example.back_qiwi.dao.SmsApproveRequest
import com.example.back_qiwi.dao.SmsResponse
import com.example.utils.models.ResponseStatus
import com.example.utils.models.ServerResponse
import okhttp3.RequestBody
import okhttp3.Response
import okhttp3.ResponseBody
import retrofit2.http.*

interface QiwiApi {
    @POST("/api/v1/payment_token/generate_sms/approve")
    suspend fun generatePaymentToken(@Body body: SmsApproveRequest): ServerResponse<PaymentResponse>

    @POST("payment_token/generate_sms")
    suspend fun generateSms(@Body body: GenerateRequest):ServerResponse<SmsResponse>

    @Streaming
    @GET("/api/v1/payment_token/generate_qr")
    suspend fun getQrCode(
        @Header("PaymentToken") paymentToken:String,
        @Header("smsToken") smsToken:String,
    ): ResponseBody

}