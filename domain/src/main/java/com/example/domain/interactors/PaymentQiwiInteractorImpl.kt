package com.example.domain.interactors

import com.example.domain_payment.base_entity.DataEntity
import com.example.domain_payment.entity.PaymentEntity
import com.example.domain_payment.entity.SmsApprove
import com.example.domain_payment.entity.SmsEntity
import com.example.domain_payment.entity.SmsGenerateEntity
import com.example.domain_payment.interactor.PaymentQiwiInteractor
import com.example.domain_payment.repository.PaymentQiwiRepository
import kotlinx.coroutines.flow.Flow

class PaymentQiwiInteractorImpl(
    private val repository: PaymentQiwiRepository
): PaymentQiwiInteractor {
    override suspend fun getSms(value: SmsGenerateEntity): Flow<DataEntity<SmsEntity>> {
        return repository.getSms(value)
    }

    override suspend fun getQrCode(value: SmsApprove): Flow<DataEntity<ByteArray>> {
        TODO("Not yet implemented")
    }

    override suspend fun payProduct(value: PaymentEntity): Flow<DataEntity<Boolean>> {
        TODO("Not yet implemented")
    }


}