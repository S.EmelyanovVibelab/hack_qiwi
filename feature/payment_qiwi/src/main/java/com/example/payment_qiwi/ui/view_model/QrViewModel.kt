package com.example.payment_qiwi.ui.view_model

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.domain_payment.base_entity.DataEntity
import com.example.domain_payment.entity.SmsGenerateEntity
import com.example.domain_payment.interactor.PaymentQiwiInteractor
import com.example.ui_components.ui_state.ViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*
import javax.inject.Inject
import javax.inject.Provider

class QrViewModel(
    private val interactor: PaymentQiwiInteractor
):ViewModel() {
    private var requestId:String = ""
    var smsState:MutableStateFlow<ViewState<String>> = MutableStateFlow(ViewState.Default(""))
        private set
    var payState:MutableStateFlow<ViewState<String>> = MutableStateFlow(ViewState.Default(""))
    private set

    fun getSms(
        siteId:String,
    ){
        requestId = UUID.randomUUID().toString()
        viewModelScope.launch(Dispatchers.IO) {
            smsState.value = ViewState.Loading(null)
            interactor.getSms(
                SmsGenerateEntity(
                    siteId = siteId,
                    requestId = requestId
            )
            ).collect{ value->
                withContext(Dispatchers.Main) {
                    when (value) {
                        is DataEntity.Success ->{
                            value.data?.also {
                                smsState.value = ViewState.Success("")
                            }?:apply {
                                smsState.value = ViewState.Error("Произошла ошибка!")
                            }
                        }
                        is DataEntity.Error ->{
                            smsState.value = ViewState.Error(value.exceptionMessage)
                        }
                    }
                }
            }
        }
    }

    fun pay(
        paymentToken:String,
        price:String,
        currency: String,
    ){

    }





    companion object{
        @Suppress("UNCHECKED_CAST")
        class Factory @Inject constructor(
            private val interactor: Provider<PaymentQiwiInteractor>,
        ) : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                require(modelClass == QrViewModel::class.java)
                return QrViewModel(interactor.get()) as T
            }
        }
    }
}