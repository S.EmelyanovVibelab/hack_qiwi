package com.example.payment_qiwi.ui

import android.Manifest
import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import com.budiyev.android.codescanner.CodeScanner
import com.budiyev.android.codescanner.DecodeCallback
import com.budiyev.android.codescanner.ErrorCallback
import com.example.dependency.findDependencies
import com.example.payment_qiwi.PaymentQiwiConst
import com.example.payment_qiwi.R
import com.example.payment_qiwi.databinding.FragmentScanQrBinding
import com.example.payment_qiwi.di.DaggerPaymentQiwiComponent
import com.example.payment_qiwi.ui.adapter.AdapterPayment
import com.example.payment_qiwi.ui.adapter.PaymentItem
import com.example.payment_qiwi.ui.view_model.QrViewModel
import com.example.ui_components.base_ui.BaseFragment
import com.example.ui_components.extension.Mode
import com.example.ui_components.extension.setEnable
import com.example.ui_components.ui_state.ViewState
import dagger.Lazy
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class FragmentQrScan : BaseFragment<FragmentScanQrBinding>(R.layout.fragment_scan_qr) {
    @Inject
    lateinit var viewModelFactory: Lazy<QrViewModel.Companion.Factory>
    private var price: String = ""
    private var currency: String = "RUB"
    private val viewModel: QrViewModel by viewModels{
        viewModelFactory.get()
    }

    override val binding: FragmentScanQrBinding by viewBinding()
    private val codeScanner: CodeScanner by lazy {
        CodeScanner(requireContext(),binding.scannerView)
    }
    private val adapterPayyment: AdapterPayment by lazy {
        AdapterPayment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        price = arguments?.getString(PaymentQiwiConst.PRICE_KEY)?:"0"
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        DaggerPaymentQiwiComponent.factory()
            .create(findDependencies())
            .inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScaner()
        initButton()
        initRV(
            listOf<PaymentItem>(
                PaymentItem(R.drawable.ic_card,resources.getString(R.string.card),resources.getString(R.string.use_client_card)),
                PaymentItem(R.drawable.ic_check,"$price ₽",resources.getString(R.string.wiil_writen)),
            )
        )
        initObserver()
    }

    private fun setupScaner(){
        codeScanner.decodeCallback = DecodeCallback {
            viewModel.pay(
                it.text,
                price,
                currency,
            )
            codeScanner.stopPreview()
            binding.frameLayout.visibility = View.GONE
        }
        codeScanner.errorCallback = ErrorCallback{
            showSnackbar(it.message?:resources.getString(R.string.error_message))
            codeScanner.stopPreview()
            binding.frameLayout.visibility = View.GONE
            binding.buttonScan.setEnable(
                true,
                resources.getColor(com.example.ui_components.R.color.color_background_button_1),
                Mode.SRC_IN,
            )
            binding.buttonScan.visibility = View.VISIBLE
        }
    }

    private fun initObserver(){
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.payState.collect{
                when(it){
                    is ViewState.Default ->{}
                    is ViewState.Error ->{
                        showSnackbar(it.localData?:resources.getString(R.string.error_message))
                        with(binding){
                            status.visibility = View.GONE
                            buttonScan.visibility = View.VISIBLE
                            buttonScan.setEnable(
                                true,
                                resources.getColor(com.example.ui_components.R.color.color_background_button_1),
                                Mode.SRC_IN,
                            )
                            frameLayout.visibility = View.GONE
                        }
                    }
                    is ViewState.Success ->{
                        binding.status.visibility = View.VISIBLE
                    }
                    is ViewState.Loading ->{
                        binding.buttonScan.setEnable(
                            false,
                            resources.getColor(com.example.ui_components.R.color.color_background_button_2),
                            Mode.SRC_IN,
                        )
                    }
                }
            }
        }
    }

    private fun initRV(list: List<PaymentItem>){
        with(binding){
            paymentRv.also{
                it.layoutManager = LinearLayoutManager(requireContext())
                it.adapter = adapterPayyment
                adapterPayyment.setData(list)
            }
        }
    }


    private fun initButton(){
        with(binding){
            buttonScan.setOnClickListener {
                cameraLauncher.launch(Manifest.permission.CAMERA)
            }
        }
    }

    private val cameraLauncher = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ){  isGranted ->
        if(isGranted){
            startScan()
        }else{
            showSnackbar(resources.getString(R.string.error_camera_per))
        }

    }

    private fun startScan(){
        with(binding){
            codeScanner.startPreview()
            buttonScan.visibility = View.INVISIBLE
            frameLayout.visibility = View.VISIBLE
        }
    }

    override fun onPause() {
        codeScanner.releaseResources()
        super.onPause()
    }

}