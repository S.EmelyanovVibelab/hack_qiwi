package com.example.payment_qiwi.di

import com.example.dependency.Dependencies
import com.example.domain_payment.interactor.PaymentQiwiInteractor
import com.example.payment_qiwi.ui.FragmentQrGenerate
import com.example.payment_qiwi.ui.FragmentQrScan
import dagger.Component

@Component(dependencies = [PaymentQiwiDeps::class])
interface PaymentQiwiComponent {
    @Component.Factory
    interface Factory {
        fun create(deps:PaymentQiwiDeps): PaymentQiwiComponent
    }
    fun inject(view: FragmentQrGenerate)
    fun inject(view: FragmentQrScan)
}

interface PaymentQiwiDeps : Dependencies{
    val paymentQiwiInteractor: PaymentQiwiInteractor
}