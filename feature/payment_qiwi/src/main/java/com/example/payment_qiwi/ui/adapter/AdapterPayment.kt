package com.example.payment_qiwi.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.payment_qiwi.databinding.ItemPaymentBinding

class AdapterPayment:RecyclerView.Adapter<AdapterPayment.PaymentHolder>() {
    private var data:MutableList<PaymentItem> = mutableListOf()

    fun setData(value: List<PaymentItem>){
        data.clear()
        data.addAll(value)
        notifyDataSetChanged()
    }

    class PaymentHolder(private val binding: ItemPaymentBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(paymentItem: PaymentItem) {
            with(binding){
                imageView.setBackgroundResource(paymentItem.image)
                title.text = paymentItem.title
                description.text = paymentItem.description
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentHolder {
        return PaymentHolder(
            binding = ItemPaymentBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        )
    }

    override fun onBindViewHolder(holder: PaymentHolder, position: Int) {
        holder.bind(
            data.get(position)
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

}

data class PaymentItem(
    val image: Int,
    val title: String,
    val description: String,
)