package com.example.payment_qiwi.ui

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.dependency.findDependencies
import com.example.payment_qiwi.R
import com.example.payment_qiwi.databinding.FragmentGenerateQrBinding
import com.example.payment_qiwi.di.DaggerPaymentQiwiComponent
import com.example.payment_qiwi.ui.adapter.AdapterPayment
import com.example.payment_qiwi.ui.adapter.PaymentItem
import com.example.payment_qiwi.ui.view_model.QrViewModel
import com.example.ui_components.base_ui.BaseFragment
import com.example.ui_components.ui_state.ViewState
import dagger.Lazy
import kotlinx.coroutines.launch
import javax.inject.Inject

class FragmentQrGenerate:BaseFragment<FragmentGenerateQrBinding>(R.layout.fragment_generate_qr){
    override val binding: FragmentGenerateQrBinding by viewBinding()
    private val adapterPayyment: AdapterPayment by lazy {
        AdapterPayment()
    }

    @Inject
    lateinit var viewModelFactory: Lazy<QrViewModel.Companion.Factory>
    private val viewModel: QrViewModel by viewModels{
        viewModelFactory.get()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRV(
            listOf(
                PaymentItem(R.drawable.ic_card, resources.getString(R.string.card),resources.getString(R.string.use_card)),
                PaymentItem(R.drawable.ic_security,resources.getString(R.string.security_title),resources.getString(R.string.security_description)),
                PaymentItem(R.drawable.ic_phone,resources.getString(R.string.phone_title),resources.getString(R.string.phone_description))
            )
        )
        initView()
        initButton()
        initObserver()
    }

    private fun initButton(){
        with(binding){
            buttonGenerateQr.setOnClickListener {
                viewModel.getSms(
                    TEST_SITE_ID
                )
                buttonGenerateQr.isEnabled = false
            }
        }
    }

    private fun initObserver(){
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.smsState.collect {
                when (it) {
                    is ViewState.Success -> {
                        with(binding){
                            buttonGenerateQr.visibility = View.GONE
                            pinView.visibility = View.VISIBLE
                            buttonApprove.visibility = View.VISIBLE
                        }
                    }
                    is ViewState.Error -> {
                        showSnackbar(it.localData?:resources.getString(R.string.error_message))
                        binding.buttonGenerateQr.isEnabled = true
                    }
                    is ViewState.Loading ->{

                    }
                    is ViewState.Default ->{

                    }
                }
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        DaggerPaymentQiwiComponent.factory()
            .create(findDependencies())
            .inject(this)
    }

    private fun initView(){
        with(binding){
            pinView.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }
                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                }
                override fun afterTextChanged(s: Editable) {}
            })
            pinView.setAnimationEnable(true)
            pinView.isCursorVisible = false
        }
    }

    private fun initRV(list: List<PaymentItem>){
        with(binding){
            paymentRv.also{
                it.layoutManager = LinearLayoutManager(requireContext())
                it.adapter = adapterPayyment
                adapterPayyment.setData(list)
            }
        }
    }
    companion object{
        private const val TEST_SITE_ID = ""
    }
}