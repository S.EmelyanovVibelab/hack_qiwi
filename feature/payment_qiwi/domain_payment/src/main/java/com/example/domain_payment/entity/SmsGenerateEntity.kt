package com.example.domain_payment.entity


data class SmsGenerateEntity(
    val requestId: String,
    val siteId:String,
)

data class SmsEntity(
    val requestId:String,
    val status: String,
)

data class SmsApprove(
    val requestId:String,
    val smsCode: String,
)