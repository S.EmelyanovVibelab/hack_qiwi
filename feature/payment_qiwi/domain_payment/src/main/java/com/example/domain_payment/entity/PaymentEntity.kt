package com.example.domain_payment.entity

data class PaymentEntity(
    val amount:String,
    val currency:String,
    val paymentId:String,
    val paymentToken: String,
    val type: String,
    val siteId: String,
)