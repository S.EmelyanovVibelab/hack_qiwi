package com.example.domain_payment.base_entity

sealed class DataEntity<out T : Any>{
    data class Success<out T : Any>(val data: T?, val code: Int) : DataEntity<T>() {
        override fun <K : Any> map(mapper: (oldValue: T?) -> K?): Success<K> {
            return Success(mapper.invoke(data), code)
        }
    }
    data class Error<out T : Any>(val exceptionMessage: String, val localData: T? = null) :
        DataEntity<T>() {
        override fun <K : Any> map(mapper: (oldValue: T?) -> K?): Error<K> {
            return Error(exceptionMessage, mapper.invoke(localData))
        }
    }
    abstract fun <K : Any> map(mapper: (oldValue: T?) -> K?): DataEntity<K>
}
