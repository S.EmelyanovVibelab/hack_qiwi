package com.example.domain_payment.repository

import com.example.domain_payment.base_entity.DataEntity
import com.example.domain_payment.entity.SmsEntity
import com.example.domain_payment.entity.SmsGenerateEntity
import kotlinx.coroutines.flow.Flow

interface PaymentQiwiRepository {
    suspend fun getSms(value: SmsGenerateEntity): Flow<DataEntity<SmsEntity>>
}