package com.example.domain_payment.interactor

import com.example.domain_payment.base_entity.DataEntity
import com.example.domain_payment.entity.PaymentEntity
import com.example.domain_payment.entity.SmsApprove
import com.example.domain_payment.entity.SmsEntity
import com.example.domain_payment.entity.SmsGenerateEntity
import kotlinx.coroutines.flow.Flow

interface PaymentQiwiInteractor {
    suspend fun getSms(value: SmsGenerateEntity): Flow<DataEntity<SmsEntity>>
    suspend fun getQrCode(value: SmsApprove): Flow<DataEntity<ByteArray>>
    suspend fun payProduct(value: PaymentEntity): Flow<DataEntity<Boolean>>
}